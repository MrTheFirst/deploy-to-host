For config auto deploy you need add config files in project.

## .gitlab-ci.yml config

Add variables in project Settings -> CI/CD:
```
SSHHOST - develop hosting
SSHLOGIN - login
SSHPASS - password
TELEGRAM_USER_ID - telegram user id for notifications 
```
Folder "dist" is copied to the root directory of the hosting. You need link a domain to the "dist" folder.

See image versions in [Progect Registry](./container_registry)

```
image: registry.rusrobots.ru/aic.develop/node-alipine-deploy/deploy-to-host:1.0

stages:
  - deploydev
  - notify

# install deployer
deploydev:
  stage: deploydev
  tags:
    - php
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - node_modules/
  before_script:
    - npm install
  script:
    - echo "start deploy"
    - npm run build
    - rsync -zrpth --delete --stats --rsh="sshpass -p $SSHPASS ssh -o StrictHostKeyChecking=no -l $SSHLOGIN" $(pwd)/dist/ SSHHOST:dist/
    - sh ./ci-notify.sh ✅
  only:
    - develop

notify_error:
  stage: notify
  script:
    - sh ./ci-notify.sh ❌
  when: on_failure
  only:
    - develop
```

## ci-notify.sh config

@GitLab_CI_Notify_bot

```
#!/bin/bash

TIME="10"
URL="https://api.mrthefirst.pro/bot/"
TEXT="Deploy status: $1%0A%0AProject:+$CI_PROJECT_NAME%0AURL:+$CI_PROJECT_URL/pipelines/$CI_PIPELINE_ID/%0ABranch:+$CI_COMMIT_REF_SLUG"

curl -s --max-time $TIME -d "chat_id=$TELEGRAM_USER_ID&disable_web_page_preview=1&text=$TEXT" $URL > /dev/null

```
