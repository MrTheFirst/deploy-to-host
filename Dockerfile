FROM node:10.15.0-alpine
RUN apk add --no-cache \
        autoconf \
        automake \
        bash \
        g++ \
        libc6-compat \
        libjpeg-turbo-dev \
        libpng-dev \
        make \
        nasm \
        openssh \
        sshpass \
        rsync \
        curl \
        ca-certificates \
        git
RUN npm i pm2 -g

